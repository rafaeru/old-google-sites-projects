# Dedicatória ao Alberto

[![VER WEBSITE](https://img.shields.io/badge/-VER%20WEBSITE-ORANGE?style=flat-square)](https://rafaeru.gitlab.io/old-google-sites-projects/dedicatoria-alberto/)

## Notas

* A [imagem de "Alberto"](images/Alberto.jpg) é de Joaquim Alberto, conhecido pelos [seus vídeos](https://www.youtube.com/results?search_query=Joaquim+Alberto).