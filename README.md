# [old-google-sites-projects](https://rafaeru.gitlab.io/old-google-sites-projects/)

[![license](https://img.shields.io/badge/license-Unlicense-7cd958)](LICENSE)

Projeto com websites originalmente criados na plataforma [*Google Sites*](https://sites.google.com/site/) (versão clássica) em setembro de 2017 recriados com o poder de [HTML](https://pt.wikipedia.org/wiki/HTML) (*HyperText Markup Language*) e  [CSS](https://pt.wikipedia.org/wiki/CSS) (*Cascading Style Sheets*).

## Lista de websites recriados

* [Dedicatória ao Alberto](/dedicatoria-alberto) \[`DA`\]
* [Memorial ao Malaquias Asiático](/memorial-malaquias-asiatico) \[`MMA`\]

**Para visualizar as páginas web acima apresentadas diriga-se à [*landing page*](https://rafaeru.gitlab.io/old-google-sites-projects/) \[`LP`\] deste projeto**.

Um backup dos ficheiros dos sites nas suas formas originais encontra-se na pasta [`old-files-backup`](/old-files-backup).

## [Licença](LICENSE)

Este projeto está licenciado sob a **The Unlicense**. [Mais informações](https://choosealicense.com/licenses/unlicense/)  
This project is licensed under the **The Unlicense**. [More information](https://choosealicense.com/licenses/unlicense/)