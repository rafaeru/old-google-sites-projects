# Backup

## Google Takeout (`./takeout/`)

Esta pasta contém os ficheiros extraídos do [*Google Takeout*](https://takeout.google.com/) compressos em ficheiros [`.7z`](https://www.7-zip.org/7z.html) para cada site.

![Transferir dados do Google Sites clássico](./2019-10-20_17-25-45.png)

Estas páginas foram geradas automaticamente pela ferramenta Google Sites e por isso estão cheias de porcaria desnecessária, principalmente código JavaScript.

![Código JavaScript desnecessário](./2022-02-27_14-48-17.png)  
Captura de ecrã que ilustra o problema.

## SingleFileZ

[SingleFileZ](https://github.com/gildas-lormeau/SingleFileZ) é uma extensão para navegadores de internet que permite transferir páginas web. Os sites também foram transferidos usando esta extensão. A extensão removeu a porcaria desnecessária referida anteriormente.

As cópias dos sites obtidas com esta extensão estão nas pastas `.dedicatoriaalberto/` e `./malaquiasasiatico/`.
